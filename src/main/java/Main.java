import utils.ImportData;
import utils.MenuCMD;
import utils.Printer;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        ImportData.saveAccessories();
        ImportData.saveConsoles();
        ImportData.saveVideoGames();
        ImportData.saveClients();
        Printer.print("**************** VIDEO GAMES STORE SYSTEM ***************");
        MenuCMD mainMenu = new MenuCMD("Main Menu", Map.of(
                1, "Register Products",
                2, "Modify Products",
                3, "Remove Products",
                4, "Search Products",
                5, "Register Clients",
                6, "Update Stock",
                7, "Get Products",
                8, "Register Sale"

        ));

        mainMenu.display();
    }
}


