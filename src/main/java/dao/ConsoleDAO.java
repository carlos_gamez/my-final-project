package dao;
import models.Accessory;
import models.Console;
import utils.Printer;
import utils.ScannerUtil;
import java.util.*;

public class ConsoleDAO implements DAO<Console>{

    ScannerUtil sc = new ScannerUtil();
    private static List<Console> consoleList = new ArrayList<>();
    public Optional<Console> find(){
        return null;
    }

    public List<Console> findAll(Boolean print){
        for (Console c : consoleList) {
            consoleList.indexOf(c);
            if (print)
                Printer.print(consoleList.indexOf(c) + ". " + c.toString());
        }
        return consoleList;
    }
    @Override
    public void save() {

        try {
            String consoleName = sc.scanText("Introduce the Console Name: ","The product could not be created");
            String brand = sc.scanText("Introduce the Console Brand: ",null);
            String model = sc.scanText("Introduce the Console Model: ",null);
            Double price = sc.scanDouble("Introduce the Price ($us). of the Console: ",null);
            String description = sc.scanText("Introduce the Console Description: ",null);
            Console newConsole = new Console(consoleName,price,description,brand,model);
            this.consoleList.add(newConsole);
            Printer.print("Product create successfully, ID: " +newConsole.getId());
        }catch (InputMismatchException e)
        {
            Printer.print("Product could not be created");
        }
    }

    public void save(Console product){
        Console newConsole = product;
        this.consoleList.add(newConsole);

    }
    public void update(Console product){
        try {
            Console updatedAcc = consoleList.get(consoleList.indexOf(product));
            String consoleName = sc.scanText("Introduce the Console Name: ", "The console product could not be created");
            String brand = sc.scanText("Introduce the Console Brand: ", null);
            String model = sc.scanText("Introduce the Console Model: ", null);
            Double price = sc.scanDouble("Introduce the Price ($us). of the Console: ", null);
            String description = sc.scanText("Introduce the Console Description: ", null);
            updatedAcc.setBrand(brand);
            updatedAcc.setProductName(consoleName);
            updatedAcc.setPrice(price);
            updatedAcc.setDescription(description);
            Printer.print("Product console updated successfully, new information " + updatedAcc.toString());
        } catch (NullPointerException e) {
            Printer.print("The Product console is invalid");
        } catch (IndexOutOfBoundsException e) {
            Printer.print("The ID for the console does not exist");
        }

    }
    public void delete(String id){
        try {
            if (sc.scanText("Are you sure to delete the following Product Console? yes/no", null).equals("yes")) {
                consoleList.remove(getProductByID(id));
                Printer.print("Product Console selected removed successfully");
            }
        } catch (IndexOutOfBoundsException e) {
            Printer.print("The Console ID does not exist");
            delete(sc.scanText("Introduce an existing Console ID that you want to delete: ", null));
        }

    }

    @Override
    public Console getProductByID(String id) {
        try {
            Console console = new Console();
            for (Console acc : consoleList) {
                if (acc.getId().equals(id))
                    console = acc;
            }
            if (consoleList.get(consoleList.indexOf(console)) != null)
            {
                Printer.print(console.toString());
                return console;
            }
            else {
                Printer.print("Console not found");
                return null;
            }

        } catch (Exception e) {
            Printer.print("The Console ID is invalid");
            return null;
        }
    }

    @Override
    public void updateStock(Integer id, Integer qty) {
        try {
            Console con = consoleList.get(id);
            con.setQuantity(qty + con.getQuantity());
            Printer.print("Stock updated successfully...");
        }catch (Exception e){
            Printer.print("Invalid option");
        }

    }
    public void salesStock(Integer id,Integer qty) {
        try {
            Console con = consoleList.get(id);
            con.setQuantity(con.getQuantity() - qty );
        }catch (Exception e){
            Printer.print("Invalid option");
        }
    }
}
