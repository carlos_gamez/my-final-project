package dao;
import java.util.List;
import java.util.Optional;

public interface DAO<T> {
    public abstract Optional<T> find();
    public abstract List<T> findAll(Boolean print);
    public abstract void save(T product);
    public abstract void save();
    public abstract void update(T product);
    public abstract void delete(String id);
    public  abstract T getProductByID(String id);

    public  abstract void updateStock(Integer id,Integer qty);
    public  abstract void salesStock(Integer id,Integer qty);

}
