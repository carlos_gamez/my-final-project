package dao;
import models.Accessory;
import models.Client;
import utils.Printer;
import utils.ScannerUtil;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;

public class ClientDAO implements DAO<Client>{
    ScannerUtil sc = new ScannerUtil();
    private static List<Client> clientList = new ArrayList<>();

    public Optional<Client> find() {
        return null;
    }

    public List<Client> findAll(Boolean print) {
        for (Client c : clientList) {
            clientList.indexOf(c);
            if (print)
                Printer.print(clientList.indexOf(c) + ". " + c.toString());
        }
        return clientList;
    }
    @Override
    public void save(Client client) {
        this.clientList.add(client);
    }
    public void save() {
        try {
            String firstName = sc.scanText("Introduce the Client First Name: ", "The client could not be registered");
            String lastName = sc.scanText("Introduce the Client Last Name: ", "The client could not be registered");
            String gender = sc.scanText("Introduce the Client Gender: ", null);
            int age = sc.scanInt("Introduce the Client Age: ", null);
            int phone = sc.scanInt("Introduce Client Phone Number: ", null);
            String address = sc.scanText("Introduce Client Address: ", null);
            Client newClient = new Client(firstName,lastName,gender,age,phone,address);
            this.clientList.add(newClient);
            Printer.print("Client registered successfully, ID: " + newClient.getClientID());
        } catch (InputMismatchException e) {
            Printer.print("Client could not be registered");
        }
    }

    @Override
    public void update(Client product) {

    }

    @Override
    public void delete(String id) {

    }

    @Override
    public Client getProductByID(String id) {
        return null;
    }

    @Override
    public void updateStock(Integer id, Integer qty) {

    }

    @Override
    public void salesStock(Integer id, Integer qty) {

    }
}
