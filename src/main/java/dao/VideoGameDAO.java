package dao;
import models.Accessory;
import models.VideoGame;
import utils.Printer;
import utils.ScannerUtil;
import java.util.*;

public class VideoGameDAO implements DAO<VideoGame> {

    ScannerUtil sc = new ScannerUtil();
    private static List<VideoGame> gameList = new ArrayList<>();
    public Optional<VideoGame> find(){
        return null;
    }

    public List<VideoGame> findAll(Boolean print){
        for (VideoGame c : gameList) {
            gameList.indexOf(c);
            if (print)
                Printer.print(gameList.indexOf(c) + ". " + c.toString());
        }
        return gameList;
    }
    @Override
    public void save() {
        try {
            String name = sc.scanText("Introduce the Game developer Company name: ","The product could not be created");
            String title = sc.scanText("Introduce the Game Title: ",null);
            String forConsole = sc.scanText("Introduce for what console is this Game compatible: ",null);
            Double price = sc.scanDouble("Introduce the Price ($us). of the Game: ",null);
            String description = sc.scanText("Introduce the Game Description: ",null);
            VideoGame newVideoGame = new VideoGame(name,price,description,title,forConsole);
            this.gameList.add(newVideoGame);
            Printer.print("Product create successfully, ID: " + newVideoGame.getId());
        }catch (InputMismatchException e)
        {
            Printer.print("Product could not be created");
        }

    }

    public void save(VideoGame product){
        VideoGame newVideoGame = product;
        this.gameList.add(newVideoGame);

    }
    public void update(VideoGame product){
        try {
            VideoGame updatedAcc = gameList.get(gameList.indexOf(product));
            String name = sc.scanText("Introduce the Game developer Company name: ", null);
            String title = sc.scanText("Introduce the Game Title: ", null);
            String forConsole = sc.scanText("Introduce for what console is this Game compatible: ", null);
            Double price = sc.scanDouble("Introduce the Price ($us). of the Game: ", null);
            String description = sc.scanText("Introduce the Game Description: ", null);
            updatedAcc.setProductName(name);
            updatedAcc.setTitle(title);
            updatedAcc.setForConsole(forConsole);
            updatedAcc.setPrice(price);
            updatedAcc.setDescription(description);

            Printer.print("Product Game updated successfully, new information " + updatedAcc.toString());
        } catch (NullPointerException e) {
            Printer.print("The Product Game is invalid");
        } catch (IndexOutOfBoundsException e) {
            Printer.print("The Game ID does not exist");
        }

    }
    public void delete(String id){
        try {
            if (sc.scanText("Are you sure to delete the following Video Game? yes/no", null).equals("yes")) {
                gameList.remove(getProductByID(id));
                Printer.print("Video Game removed successfully");
            }
        } catch (IndexOutOfBoundsException e) {
            Printer.print("The Video Game ID does not exist");
            delete(sc.scanText("Introduce an existing Video Game ID that you want to delete: ", null));
        }

    }

    @Override
    public VideoGame getProductByID(String id) {
        try {
            VideoGame game = new VideoGame();
            for (VideoGame acc : gameList) {
                if (acc.getId().equals(id))
                    game = acc;
            }
            if (gameList.get(gameList.indexOf(game)) != null)
            {
                Printer.print(game.toString());
                return game;
            }
            else {
                Printer.print("Video Game not found");
                return null;
            }

        } catch (Exception e) {
            Printer.print("The Video Game ID is invalid");
            return null;
        }
    }

    @Override
    public void updateStock(Integer id, Integer qty) {
        try {
            VideoGame gam = gameList.get(id);
            gam.setQuantity(qty + gam.getQuantity());
            Printer.print("Stock updated successfully...");
        }catch (Exception e){
            Printer.print("Invalid option");
        }

    }
    public void salesStock(Integer id,Integer qty) {
        try {
            VideoGame vgam = gameList.get(id);
            vgam.setQuantity(vgam.getQuantity() - qty );
        }catch (Exception e){
            Printer.print("Invalid option");
        }
    }
}
