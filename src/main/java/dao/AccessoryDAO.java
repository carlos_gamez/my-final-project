package dao;
import models.Accessory;
import models.VideoGame;
import utils.Printer;
import utils.ScannerUtil;
import java.util.*;

public class AccessoryDAO implements DAO<Accessory> {

    ScannerUtil sc = new ScannerUtil();
    private static List<Accessory> accessoryList = new ArrayList<>();

    public Optional<Accessory> find() {
        return null;
    }

    public List<Accessory> findAll(Boolean print) {
        for (Accessory c : accessoryList) {
            accessoryList.indexOf(c);
            if (print)
                Printer.print(accessoryList.indexOf(c) + ". " + c.toString());
        }
        return accessoryList;
    }

    @Override
    public void save(Accessory product) {
        this.accessoryList.add(product);
    }

    public void save() {
        try {
            String brand = sc.scanText("Introduce the Accessory Brand: ", "The product could not be created");
            String productName = sc.scanText("Introduce the Accessory Name: ", null);
            String type = sc.scanText("Introduce the Accessory Type: ", null);
            String forConsole = sc.scanText("Introduce for what console is this accessory compatible: ", null);
            Double price = sc.scanDouble("Introduce the Price ($us). of the Product: ", null);
            String description = sc.scanText("Introduce the Accessory Description: ", null);
            Accessory newAccessory = new Accessory(productName, price, description);
            this.accessoryList.add(newAccessory);
            Printer.print("Product create successfully, ID: " + newAccessory.getId());
        } catch (InputMismatchException e) {
            Printer.print("Product could not be created");
        }
    }

    public void update(Accessory product) {
        try {
            Accessory updatedAcc = accessoryList.get(accessoryList.indexOf(product));
            String brand = sc.scanText("Introduce the Accessory Brand: ", "The product could not be created");
            String productName = sc.scanText("Introduce the Accessory Name: ", null);
            String type = sc.scanText("Introduce the Accessory Type: ", null);
            String forConsole = sc.scanText("Introduce for what console is this accessory compatible: ", null);
            Double price = sc.scanDouble("Introduce the Price ($us). of the Product: ", null);
            String description = sc.scanText("Introduce the Accessory Description: ", null);
            updatedAcc.setBrand(brand);
            updatedAcc.setProductName(productName);
            updatedAcc.setType(type);
            updatedAcc.setForConsole(forConsole);
            updatedAcc.setPrice(price);
            updatedAcc.setDescription(description);

            Printer.print("Product Accessory updated successfully, new information " + updatedAcc.toString());
        } catch (NullPointerException e) {
            Printer.print("The Product Accessory is invalid");
        } catch (IndexOutOfBoundsException e) {
            Printer.print("The Accessory ID does not exist");
        }

    }

    public void delete(String id) {
        try {
            if (sc.scanText("Are you sure to delete the following Accessory? yes/no", null).equals("yes")) {
                accessoryList.remove(getProductByID(id));

                Printer.print("Accessory removed successfully");
            }
        } catch (IndexOutOfBoundsException e) {
            Printer.print("The Accessory ID does not exist");
            delete(sc.scanText("Introduce an existing Accessory ID that you want to delete: ", null));
        }
    }

    @Override
    public Accessory getProductByID(String id) {
        try {
            Accessory accessory = new Accessory();
            for (Accessory acc : accessoryList) {
                if (acc.getId().equals(id))
                    accessory = acc;
            }
            if (accessoryList.get(accessoryList.indexOf(accessory)) != null)
            {
                Printer.print(accessory.toString());
                return accessory;
            }
            else {
                Printer.print("Accessory not found");
                return null;
            }

        } catch (Exception e) {
            Printer.print("The Accessory ID is invalid");
            return null;
        }

    }

    @Override
    public void updateStock(Integer id,Integer qty) {
        try {
            Accessory acc = accessoryList.get(id);
            acc.setQuantity(qty + acc.getQuantity());
            Printer.print("Stock updated successfully...");
        }catch (Exception e){
            Printer.print("Invalid option");
        }
    }

    public void salesStock(Integer id,Integer qty) {
        try {
            Accessory acc = accessoryList.get(id);
            acc.setQuantity(acc.getQuantity() - qty );
        }catch (Exception e){
            Printer.print("Invalid option");
        }
    }



}
