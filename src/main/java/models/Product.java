package models;
import utils.IDGenerator;


public abstract class Product {
    String id = IDGenerator.generate();
    String productName;
    Double price;
    String description;
    Integer quantity = 0;

    public Product(Product newProduct) {
        this.id = newProduct.getId();
        this.productName = newProduct.getProductName();
        this.price = newProduct.getPrice();
        this.description = newProduct.getDescription();
        this.quantity = newProduct.getQuantity();
    }

    public String getProductName() {
        return productName;
    }

    public String getDescription() {
        return description;
    }

    public Product()
    {

    }
    public Product( String productName, Double price, String description) {
        this.productName = productName;
        this.price = price;
        this.description = description;
    }

    public Product(String productName, Double price, String description, Integer quantity) {
        this.productName = productName;
        this.price = price;
        this.description = description;
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public abstract String toString();

    public Double getPrice() {
        return price;
    }
}
