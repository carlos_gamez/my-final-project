package models;

public class Console extends Product {
    private String brand;
    private String model;

    public Console( String productName, Double price, String description) {
        super(productName, price, description);
    }

    public Console( String productName, Double price, String description, String brand, String model) {
        super(productName, price, description);
        this.brand = brand;
        this.model = model;
    }

    public Console(Console newProduct) {
        super(newProduct);
        this.brand = newProduct.getBrand();
        this.model = newProduct.getModel();
    }

    public Console() {}

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    @Override
    public String toString() {
        String output = "id: " + id + " | "+
                " Qty: " + quantity + " | "+
                " Brand: " + brand + " | "+
                " Product Name: " + productName + " | "+
                " Model: " + model + " | "+
                " Price($us): " + price + " | "+
                " Description: " + description;
        return output;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
