package models;

public class VideoGame extends Product{
    private String title;
    private String forConsole;

    public VideoGame() {}

    public VideoGame(VideoGame newProduct) {
        super(newProduct);
        this.title = newProduct.getTitle();
        this.forConsole = newProduct.getForConsole();
    }

    public String getTitle() {
        return title;
    }

    public String getForConsole() {
        return forConsole;
    }

    public VideoGame(String productName, Double price, String description, String title, String forConsole) {
        super(productName, price, description);
        this.title = title;
        this.forConsole = forConsole;

    }

    @Override
    public String toString() {
        String output = "id: " + id + " | "+
                " Qty: " + quantity + " | "+
                " Product Name: " + productName + " | "+
                " Title: " + title + " | "+
                " For console: " + forConsole + " | "+
                " Price($us): " + price + " | "+
                " Description: " + description;
        return output;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setForConsole(String forConsole) {
        this.forConsole = forConsole;
    }
}
