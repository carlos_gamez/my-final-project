package models;

import dao.AccessoryDAO;
import dao.DAO;
import utils.IDGenerator;
import utils.Printer;
import utils.ScannerUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class Sale {
    private Client client;
    private List<Product> products;
    private List<Console> consoles = new ArrayList<Console>();;
    private List<VideoGame> games = new ArrayList<VideoGame>();;
    private List<Accessory> accessories = new ArrayList<Accessory>();
    private String id = IDGenerator.generate();
    private double amount;

    protected Sale(Client client, List<Product> product) {
        this.client = client;
        this.products = product;
    }

    public Sale() {
    }

    public Sale(Client client, List<Console> consoles, List<VideoGame> games, List<Accessory> accessories) {
        this.client = client;
        this.consoles = consoles;
        this.games = games;
        this.accessories = accessories;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void registerSale(DAO dao) {
        int productIndex;
        int productQty;
        dao.findAll(true);
        ScannerUtil sc = new ScannerUtil();
        String category = dao.getClass().getName();

        switch (category) {
            case "dao.AccessoryDAO":
                productIndex = sc.scanInt("Select the product for the sale: ", null);
                Accessory acc = new Accessory();
                try {
                    acc = new Accessory((Accessory) dao.findAll(false).get(productIndex));
                } catch (IndexOutOfBoundsException e) {
                    Printer.print("Invalid Option");
                }
                productQty = sc.scanInt("Introduce quantity of the selected product to sale", null);
                if (acc.getQuantity() >= productQty) {
                    dao.salesStock(productIndex, productQty);
                    acc.setQuantity(productQty);
                    accessories.add(acc);
                } else {
                    Printer.print("Invalid quantity");
                }
                break;

            case "dao.ConsoleDAO":
                productIndex = sc.scanInt("Select the product for the sale: ", null);
                Console con = new Console();
                try {
                    con = new Console((Console) dao.findAll(false).get(productIndex));
                } catch (IndexOutOfBoundsException e) {
                    Printer.print("Invalid Option");
                }
                productQty = sc.scanInt("Introduce quantity of the selected product to sale", null);
                if (con.getQuantity() >= productQty) {
                    dao.salesStock(productIndex, productQty);
                    con.setQuantity(productQty);
                    consoles.add(con);
                } else {
                    Printer.print("Invalid quantity");
                }
                break;
                case "dao.VideoGameDAO":
                productIndex = sc.scanInt("Select the product for the sale: ", null);
                VideoGame game = new VideoGame();
                try {
                    game = new VideoGame((VideoGame) dao.findAll(false).get(productIndex));
                } catch (IndexOutOfBoundsException e) {
                    Printer.print("Invalid Option");
                }
                productQty = sc.scanInt("Introduce quantity of the selected product to sale", null);
                if (game.getQuantity() >= productQty) {
                    dao.salesStock(productIndex, productQty);
                    game.setQuantity(productQty);
                    games.add(game);
                } else {
                    Printer.print("Invalid quantity");
                }
                break;

        }


    }

    public void registerSale() {
        AccessoryDAO accessoriesDAO = new AccessoryDAO();
        accessoriesDAO.findAll(true);
        ScannerUtil sc = new ScannerUtil();
        int productIndex = sc.scanInt("Select the product for the sale: ", null);
        int productQty = sc.scanInt("Introduce quantity of the selected product to sale", null);

        try {
            Accessory acc = accessoriesDAO.findAll(false).get(productIndex);
//        Adding product to sales
            Accessory newAcc = new Accessory(acc);
            if (productQty <= acc.getQuantity()) {
                newAcc.setQuantity(productQty);
//        Removing stock from Accessories list
                accessoriesDAO.salesStock(productIndex, productQty);
            } else {
                Printer.print("Invalid quantity");
            }
            this.accessories.add(newAcc);
            listValues();
            accessoriesDAO.findAll(true);
        } catch (IndexOutOfBoundsException e) {
            Printer.print("Invalid option ");
        }
        calculateAmount();
    }

    public void calculateAmount() {

        Double total = 0.0;
        for (Accessory acc : this.accessories) {
            total = total + (acc.getQuantity() * acc.getPrice());
        }
        for (Console con:this.consoles) {
            total = total + (con.getQuantity() * con.getPrice());
        }
        for (VideoGame game:this.games) {
            total = total + (game.getQuantity() * game.getPrice());
        }
        this.amount = total;
        Printer.print(String.valueOf(amount));
    }

    public void listValues() {
        Printer.print("\n -- Purchase information: -- ");
        try {
            for (Accessory acc : this.accessories) {
                Printer.print(acc.toString());
            }
            for (Console con : this.consoles) {
                Printer.print(con.toString());
            }
            for (VideoGame game : this.games) {
                Printer.print(game.toString());
            }
            calculateAmount();
            Printer.print("\n -- Total Amount($us).- "+this.amount);

        }catch (NullPointerException e){

        }
    }

    public void printInvoice() {
        String p = new File("").getAbsolutePath();
        String fileName = "Invoice(Service Payment Project).txt";
        String absolutePath = p.concat("/src/main/resources/products.json") + "\\" + fileName;
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);

        //Write the content in file
        try (FileOutputStream fileOutputStream = new FileOutputStream(absolutePath)) {
            String fileContent = "************* INVOICE SERVICES PAYMENT *************"
                    + "\r\n DETAILS"
                    + "\r\n Invoice ID: " + id
                    + "\r\n"
                    + "\r\n Company: " + "VIDEO GAMES STORE S.R.L."
                    + "\r\n Customer Name: " + this.client.getFirstName() + " " + this.client.getLastName()
                    + "\r\n Product Purchased: "
                    + "\r\n Total Amount Paid: " + "$us. " + df.format(this.amount)
                    + "\r\n Thank you for your purchase!!"
                    + "\r\n**************************************************";

            fileOutputStream.write(fileContent.getBytes());
        } catch (FileNotFoundException e) {
        } catch (IOException E) {
        }

    }
}