package models;

import utils.IDGenerator;

public class Client {
    private String clientID = IDGenerator.generate();
    private String firstName;
    private String lastName;
    private String gender;
    private int age;
    private int phone;
    private String address;

    public Client(String firstName, String lastName, String gender, int age, int phone, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
        this.phone = phone;
        this.address = address;
    }
    public Client() {
    }
    @Override
    public String toString() {
        String output = "Client ID: " + clientID +
                " Client First Name: " + firstName +
                " Client Last Name: " + lastName +
                " Gender: " + gender +
                " Age: " + age +
                " Phone: " + phone +
                " Address: " + address;
        return output;
    }

    public String getClientID() {
        return clientID;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }
}
