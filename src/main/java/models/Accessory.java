package models;

public class Accessory extends Product {
    String brand;
    String type;
    String forConsole;


    public Accessory(String productName, Double price, String description) {
        super(productName, price, description);
    }

    public Accessory(Accessory newProduct) {
        super(newProduct);
        this.brand = newProduct.getBrand();
        this.type = newProduct.getType();
        this.forConsole = newProduct.getForConsole();
    }

    public Accessory(String productName, Double price, String description, String brand, String type, String forConsole) {

        super(productName, price, description);
        this.brand = brand;
        this.type = type;
        this.forConsole = forConsole;
    }

    public Accessory() {
    }

    public String getBrand() {
        return brand;
    }

    public String getType() {
        return type;
    }

    public String getForConsole() {
        return forConsole;
    }

    @Override
    public String toString() {
        String output = "id: " + id + " | "+
                " Qty: " + quantity + " | "+
                " Product Name: " + productName + " | "+
                " Price($us): " + price + " | "+
                " Description: " + description + " | "+
                " Brand: " + brand + " | "+
                " Type: " + type + " | "+
                " For console: " + forConsole;
        return output;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setForConsole(String forConsole) {
        this.forConsole = forConsole;
    }
}
