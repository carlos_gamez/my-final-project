package utils;

import com.google.gson.Gson;
import dao.AccessoryDAO;
import dao.ClientDAO;
import dao.ConsoleDAO;
import dao.VideoGameDAO;
import models.Accessory;
import models.Client;
import models.Console;
import models.VideoGame;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.io.File;
import java.io.FileReader;

public class ImportData {
    public static void saveAccessories(){
        // p retrieves the absolute path
        String p = new File("").getAbsolutePath();
        // gson casts a json to any type class
        Gson gson = new Gson();
        // json array stores the jsonfile objects in an array format
        JSONArray jsonArray = new JSONArray();
        // parser will convert an external file to a JSONArray format
        JSONParser parser = new JSONParser();
        AccessoryDAO dao = new AccessoryDAO();
        try {
            jsonArray = (JSONArray) parser.parse(new FileReader(p.concat("/src/main/resources/accessories_data.json")));
            for (Object json:jsonArray) {
                Accessory acc = gson.fromJson(((JSONObject) json).toString(), Accessory.class);
                dao.save(acc);
            }
        }catch (Exception e){
            Printer.print(e.toString());
            Printer.print(e.getStackTrace().toString());
        }
    }
    public static void saveConsoles(){
        // p retrieves the absolute path
        String p = new File("").getAbsolutePath();
        // gson casts a json to any type class
        Gson gson = new Gson();
        // json array stores the jsonfile objects in an array format
        JSONArray jsonArray = new JSONArray();
        // parser will convert an external file to a JSONArray format
        JSONParser parser = new JSONParser();
        ConsoleDAO dao = new ConsoleDAO();
        try {
            jsonArray = (JSONArray) parser.parse(new FileReader(p.concat("/src/main/resources/consoles_data.json")));
            for (Object json:jsonArray) {
                Console con = gson.fromJson(((JSONObject) json).toString(), Console.class);
                dao.save(con);
            }
        }catch (Exception e){
            Printer.print(e.toString());
            Printer.print(e.getStackTrace().toString());
        }
    }
    public static void saveVideoGames(){
        // p retrieves the absolute path
        String p = new File("").getAbsolutePath();
        // gson casts a json to any type class
        Gson gson = new Gson();
        // json array stores the jsonfile objects in an array format
        JSONArray jsonArray = new JSONArray();
        // parser will convert an external file to a JSONArray format
        JSONParser parser = new JSONParser();
        VideoGameDAO dao = new VideoGameDAO();
        try {
            jsonArray = (JSONArray) parser.parse(new FileReader(p.concat("/src/main/resources/video_games_data.json")));
            for (Object json:jsonArray) {

                VideoGame acc = gson.fromJson(((JSONObject) json).toString(), VideoGame.class);
                dao.save(acc);
            }
        }catch (Exception e){
            Printer.print(e.toString());
            Printer.print(e.getStackTrace().toString());
        }
    }

    public static void saveClients(){
        // p retrieves the absolute path
        String p = new File("").getAbsolutePath();
        // gson casts a json to any type class
        Gson gson = new Gson();
        // json array stores the jsonfile objects in an array format
        JSONArray jsonArray = new JSONArray();
        // parser will convert an external file to a JSONArray format
        JSONParser parser = new JSONParser();

        ClientDAO dao = new ClientDAO();
        try {

            jsonArray = (JSONArray) parser.parse(new FileReader(p.concat("/src/main/resources/clients_data.json")));
            for (Object json:jsonArray) {

                Client cli = gson.fromJson(((JSONObject) json).toString(), Client.class);
                dao.save(cli);
            }

        }catch (Exception e){
            Printer.print(e.toString());
            Printer.print(e.getStackTrace().toString());
        }
    }
}
