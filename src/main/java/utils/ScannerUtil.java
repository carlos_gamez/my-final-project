package utils;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ScannerUtil {
    Scanner sc = new Scanner(System.in);

    public String scanText(String message, String errorMessage) {
        String scannedText;
        try {
            do {
                Printer.print(message);

                scannedText = sc.nextLine();

                if (scannedText== null || scannedText.equals(" ") || scannedText.equals(""))
                    Printer.print("Please introduce the requested field");
            }while (scannedText== null || scannedText.equals(" ") ||scannedText.equals(""));

                return scannedText;

        } catch (InputMismatchException e) {
            if (errorMessage != null)
                Printer.print(errorMessage);
            return null;

        }

    }

    public Double scanDouble(String message, String errorMessage){
        String scannedText;
        try {
            do {
                Printer.print(message);
                scannedText = sc.nextLine();
                if (!scannedText.matches("-?\\d+(\\.\\d+)?") )
                    Printer.print("Invalid input");
            }while (scannedText == null || !scannedText.matches("-?\\d+(\\.\\d+)?"));
            return Double.parseDouble(scannedText);
        } catch (InputMismatchException e) {
            if (errorMessage != null)
                Printer.print(errorMessage);
            return null;

        }
    }
    public int scanInt(String message, String errorMessage){
        String scannedText;
        try {
            do {
                Printer.print(message);
                scannedText = sc.nextLine();
                if (!scannedText.matches("-?\\d+(\\.\\d+)?") )
                    Printer.print("Invalid input");
            }while (scannedText == null || !scannedText.matches("-?\\d+(\\.\\d+)?"));
            return Integer.parseInt(scannedText);
        } catch (InputMismatchException e) {
            if (errorMessage != null)
                Printer.print(errorMessage);
            return 0;

        }
    }
}
