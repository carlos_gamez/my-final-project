package utils;
import dao.*;
import models.Accessory;
import models.Client;
import models.Sale;

import java.io.InputStream;
import java.util.Scanner;
import java.util.Map;

public class MenuCMD {
    String title;
    Map<Integer, String> options;
    ScannerUtil scanner = new ScannerUtil();
    Scanner sc = new Scanner(System.in);

    public MenuCMD(String title, Map<Integer, String> options) {
        this.title = title;
        this.options = options;
    }

    public void display() {
        Printer.print(this.title);
        for (int i = 1; i <=options.size() ; i++) {
            options.get(i);
            Printer.print(i + "." + " " + this.options.get(i));
        }
        int menuOption = sc.nextInt();
        executeAction(menuOption);
    }
    private void returnOption(){
        Printer.print(" \n -- Return to main menu? yes/no");
        Scanner sc = new Scanner(System.in);
        String response=sc.next();
        if (response.equals("yes"))
            Printer.clear();
            new MenuCMD("Main Menu", Map.of(
                    1, "Register Products",
                    2, "Modify Products",
                    3, "Remove Products",
                    4, "Search Products",
                    5, "Register Clients",
                    6, "Update Stock",
                    7, "Get Products",
                    8, "Register Sale"

            )).display();

    }
    private void executeAction(int option) {
        DAO product;
        SubMenuCMD subMenu = new SubMenuCMD("Choose a category", Map.of(
                1, "Consoles",
                2, "VideoGames",
                3, "Accessories"
        ));
        switch (option) {
            case 1:
                product = getCategory();
                product.save();
                returnOption();
                break;
            case 2:

                product = getCategory();
                String id = scanner.scanText("Introduce an existing product ID: ",null);
                product.update(product.getProductByID(id));

                returnOption();

                break;


            case 3:
                product= getCategory();
                product.delete(scanner.scanText("Introduce an existing product ID that you want to delete: ",null));

                returnOption();
            break;
            case 4:
                product= getCategory();
                product.getProductByID(scanner.scanText("Introduce an existing product ID: ",null));

                returnOption();

                break;
            case 5:
                ClientDAO client = new ClientDAO();
                client.save();
                returnOption();
                break;
            case 6:
                product = getCategory();
                product.findAll(true);
                Integer opt = Integer.parseInt(scanner.scanText("Select an option for the product you want to add stock",null));
                Integer qty = Integer.parseInt(scanner.scanText("Introduce the quantity:",null));
                product.updateStock(opt,qty);
                returnOption();
                break;
            case 7:
                product = getCategory();
                product.findAll(true);
                returnOption();
                break;
            case 8:
                Sale sales = new Sale();
                String addProductsQuestion ="yes";

                while (addProductsQuestion.equals("yes")) {

                    sales.registerSale(getCategory());
                    addProductsQuestion = scanner.scanText("Do you want to add more products?", null);
                }
                sales.listValues();
                returnOption();
                break;

        }

    }
    private DAO getCategory(){
        SubMenuCMD subMenu = new SubMenuCMD("Choose a category", Map.of(
                1, "Consoles",
                2, "VideoGames",
                3, "Accessories"
        ));
        subMenu.display();
        int menuOption = sc.nextInt();
        if (menuOption == 1)
            return new ConsoleDAO();
        else if (menuOption == 2)
            return new VideoGameDAO();
        else
            return new AccessoryDAO();

    }

    private class SubMenuCMD extends MenuCMD {
        protected SubMenuCMD(String title, Map<Integer, String> options) {
            super(title, options);
        }

        @Override
        public void display() {
            Printer.print(this.title);
            for (int i = 1; i <=options.size() ; i++) {
                options.get(i);
                Printer.print(i + "." + " " + this.options.get(i));
            }


        }
    }
}