
import dao.AccessoryDAO;
import models.Accessory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class AccessoryDAOTest {

    private AccessoryDAO accessoryDAO;
    private List<Accessory> accessoryList;
    @BeforeEach
    public void setup(){
        this.accessoryDAO = new AccessoryDAO();
        this.accessoryList = new ArrayList<Accessory>();
    }

    @Test
    @DisplayName("Adding new products")
    public void testSave(){
//        ScannerUtil mockScanner = mock(ScannerUtil.class);
        //set up the scanner
        AccessoryDAO accessoryDAO = new AccessoryDAO();
//        when(mockScanner.scanText("Introduce the Accessory Brand: ","The product could not be created")).thenReturn("Test Brand");
        accessoryDAO.save(new Accessory("Test Product Name",123.00,"test description"));
        accessoryDAO.save(new Accessory("Test Product Name",123.00,"test description"));
        accessoryDAO.save(new Accessory("Test Product Name",123.00,"test description"));
        accessoryDAO.save(new Accessory("Test Product Name",123.00,"test description"));
        accessoryDAO.save(new Accessory("Test Product Name",123.00,"test description"));
//        this.accessoryList=this.accessoryDAO.findAll();
        Assert.assertEquals(5,accessoryDAO.findAll(false).size());


    }
    @Test
    public void testUpdate(){
        AccessoryDAO accessoryDAO = new AccessoryDAO();
        Accessory acc= new Accessory("Test Product Name",123.00,"test description");
        accessoryDAO.save(acc);




    }
    @Test
    @DisplayName("Testing delete functionality .." )
    public void testDelete(){
        AccessoryDAO accessoryDAO = new AccessoryDAO();
        Accessory acc= new Accessory("Test Product Name",123.00,"test description");
        Accessory acc2= new Accessory("Test Product Name",123.00,"test description");
        accessoryDAO.save(acc);
        accessoryDAO.save(acc2);

        accessoryDAO.delete(acc.getId());
Assert.assertEquals(1,accessoryDAO.findAll(false).size());

    }

}
